package server;

import com.google.gson.Gson;
import comman.event.Event;
import comman.request.Request;
import comman.response.Response;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.*;

@ServerEndpoint("/chat")
public class MessageHandler {
    private static HashMap<Session, String> onlineUsers = new HashMap<Session, String>();
    private static List<Event> events = new ArrayList<Event>();
    private Gson gson = new Gson();

    @OnOpen
    public void onOpen(Session session) {
        System.out.println("WebSocket opened: " + session.getId());
    }
    @OnMessage
    public void onMessage(String txt, Session session) throws IOException {
        System.out.println("Response received: " + txt);

        Request request = gson.fromJson(txt, Request.class);
        switch (request.getType()){
            case MESSAGE:
                messageHandler(request, session);
                break;
            case LOGIN:
                loginHandler(request, session);
                break;
            case LOGOUT:
                session.close();
                break;
        }
    }

    @OnClose
    public void onClose(CloseReason reason, Session session) {
        System.out.println("Closing a WebSocket due to " + reason.getReasonPhrase());
        logoutHandler(session);
    }

    private void logoutHandler(Session session) {
        System.out.println("Logout: " + session);
        Response userLeft = new Response().createLeftEvent(onlineUsers.get(session));
        System.out.println("User left chat: " + userLeft.getEvent().getName());
        notifyEveryone(gson.toJson(userLeft), session);
        events.add(userLeft.getEvent());
        onlineUsers.remove(session);
    }

    private void messageHandler(Request messageRequest, Session sender) {
        String author = onlineUsers.get(sender);
        Response newMessage = new Response();

        newMessage.createMessageEvent(author, messageRequest.getMessage());
        events.add(newMessage.getEvent());
        String json = gson.toJson(newMessage);

        notifyEveryone(json, sender);
    }

    private void loginHandler(Request request, Session sender) throws IOException {
        String name = request.getName();

        if(onlineUsers.containsValue(name)){
            String json = gson.toJson(new Response().createBusyNameResponse());
            sender.getBasicRemote().sendText(json);
            return;
        }

        Response userJoin = new Response().createJoinEvent(name);
        events.add(userJoin.getEvent());
        String json = gson.toJson(userJoin);
        onlineUsers.put(sender, name);
        notifyEveryone(json, sender);

        json = gson.toJson(new Response().createSuccessLoginResponse());
        sender.getBasicRemote().sendText(json);


        json = gson.toJson(new Response().createUsersResponse(onlineUsers.values()));
        sender.getBasicRemote().sendText(json);

        json = gson.toJson(new Response().createLastEvents(events));
        sender.getBasicRemote().sendText(json);
    }

    private void notifyEveryone(String message, Session sender) {
        for(Session session : onlineUsers.keySet()){
            if(session.equals(sender)){
                continue;
            }
            try{
                session.getBasicRemote().sendText(message);
            }
            catch (Exception e){
                System.out.println("EXCEPTION: " + e.getMessage());
            }
        }
    }
}
