package client;

import comman.event.Event;
import comman.event.EventType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.websocket.ContainerProvider;
import javax.websocket.WebSocketContainer;

import static comman.event.EventType.MESSAGE;
import static comman.event.EventType.USER_LEFT;

public class WebSocket356ClientMain implements Observer {
    public static void main(String[] args) {
        new WebSocket356ClientMain().go();
    }

    public void go(){
        try {
            final Collection<String> onlineUsers = new ArrayList<String>();

            String dest = "ws://localhost:8080/chat";
            final ClientSocket socket = new ClientSocket(onlineUsers,this);
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(socket, new URI(dest));

            socket.getLatch().await();

            Thread readThread = new Thread(new Runnable() {
                public void run() {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                    while (!Thread.currentThread().isInterrupted()) {
                        try {
                            while (!reader.ready()){
                                Thread.sleep(200);
                            }
                            String string = reader.readLine();
                            if (string.equalsIgnoreCase("online users")) {
                                writeOnlineUsers(onlineUsers);
                                continue;
                            }
                            socket.sendMessage(string);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            return;
                        }
                    }
                }
            });

            socket.setReadThread(readThread);
            readThread.start();

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void newMessage(String name, String data) {
        System.out.println("From: " + name);
        System.out.println("Message: " + data);
    }

    public void newUser(String name) {
        System.out.println(name + " join chat");
    }

    public void leftUser(String name) {
        System.out.println(name + " left chat");
    }

    public void successLogin() {
        System.out.println("Success login!");
    }

    public void busyNameLogin() {
        System.out.println("This name is busy. Write another one");
    }

    public void lastEvents(List<Event> events) {
        for(Event event : events){
            //System.out.println(name);
            if(event.getType() == EventType.USER_JOIN){
                newUser(event.getName());
            } else if(event.getType() == USER_LEFT){
                leftUser(event.getName());
            } else if(event.getType() == MESSAGE){
                newMessage(event.getName(), event.getMessage());
            }
        }
    }

    private void writeOnlineUsers(Collection<String> users){
        System.out.println("Online users:");
        for(String name : users){
            System.out.println(name);
        }
    }
}