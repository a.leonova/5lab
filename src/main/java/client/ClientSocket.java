package client;

import com.google.gson.Gson;
import comman.event.Event;
import comman.request.Request;
import comman.response.Response;

import javax.websocket.*;
import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.CountDownLatch;

@ClientEndpoint
public class ClientSocket {
    CountDownLatch latch = new CountDownLatch(1);
    private Collection<String> onlineUsers;
    private Session session;
    private boolean login = false;
    private Observer observer;
    private Thread readThread;

    public ClientSocket(Collection<String> onlineUsers, Observer observer) {
        this.onlineUsers = onlineUsers;
        this.observer = observer;
    }

    public void setReadThread(Thread readThread) {
        this.readThread = readThread;
    }

    @OnOpen
    public void onOpen(Session session) {
        System.out.println("Connected to server");
        this.session = session;
        latch.countDown();
    }

    @OnMessage
    public void onText(String message, Session session) {
        System.out.println("Response received from server:" + message);
        Gson gson = new Gson();
        Response response = gson.fromJson(message, Response.class);
        switch (response.getType()){
            case EVENT:
                eventHandler(response.getEvent());
                break;
            case SUCCESS_LOGIN:
                login = true;
                observer.successLogin();
                break;
            case BUSY_NAME:
                observer.busyNameLogin();
                break;
            case USERS:
                onlineUsers.addAll(response.getOnlineUsers());
            case LAST_EVENTS:
                observer.lastEvents(response.getEvents());
        }
    }

    @OnClose
    public void onClose(CloseReason reason, Session session) {
        System.out.println("Closing a WebSocket due to " + reason.getReasonPhrase());
        readThread.interrupt();

    }


    private void eventHandler(Event event){
        switch (event.getType()){
            case MESSAGE:
                observer.newMessage(event.getName(), event.getMessage());
                break;
            case USER_JOIN:
                observer.newUser(event.getName());
                onlineUsers.add(event.getName());
                break;
            case USER_LEFT:
                observer.leftUser(event.getName());
                onlineUsers.remove(event.getName());
                break;
        }
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    public void sendMessage(String str) {
        try {
            Gson gson = new Gson();
            if(login){
                if(str.equalsIgnoreCase("logout")){
                    session.getBasicRemote().sendText(new Gson().toJson(new Request().createLogout()));
                    session.close();
                    return;
                }
                session.getBasicRemote().sendText(gson.toJson(new Request().createMessageRequest(str)));
            }
            else{
                session.getBasicRemote().sendText(gson.toJson(new Request().createLoginRequest(str)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
