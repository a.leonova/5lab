package client;

import comman.event.Event;
import java.util.List;

public interface Observer {
    void newMessage(String name, String data);
    void newUser(String name);
    void leftUser(String name);
    void successLogin();
    void busyNameLogin();
    void lastEvents(List<Event> events);
}
