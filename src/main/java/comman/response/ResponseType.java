package comman.response;

public enum ResponseType {
    BUSY_NAME,
    SUCCESS_LOGIN,
    USERS,
    LAST_EVENTS,
    EVENT
}
