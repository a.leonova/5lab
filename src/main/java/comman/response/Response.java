package comman.response;

import comman.event.Event;

import java.util.Collection;
import java.util.List;

public class Response {
    private ResponseType type;
    private Collection<String> onlineUsers;
    private List<Event> events;
    private Event event;

    public Response createUsersResponse(Collection<String> onlineUsers){
        type = ResponseType.USERS;
        this.onlineUsers = onlineUsers;
        return this;
    }

    public Response createSuccessLoginResponse(){
        type = ResponseType.SUCCESS_LOGIN;
        return this;
    }

    public Response createBusyNameResponse(){
        type = ResponseType.BUSY_NAME;
        return this;
    }

    public Response createMessageEvent(String name, String data){
        type = ResponseType.EVENT;
        event = new Event().createMessageEvent(name, data);
        return this;
    }

    public Response createJoinEvent(String name){
        type = ResponseType.EVENT;
        event = new Event().createJoinEvent(name);
        return this;
    }

    public Response createLeftEvent(String name){
        type = ResponseType.EVENT;
        event = new Event().createLeftEvent(name);
        return this;
    }

    public Response createLastEvents(List<Event> events){
        type = ResponseType.LAST_EVENTS;
        this.events = events;
        return this;
    }

    public List<Event> getEvents() {
        return events;
    }

    public ResponseType getType() {
        return type;
    }

    public Collection<String> getOnlineUsers() {
        return onlineUsers;
    }

    public Event getEvent() {
        return event;
    }
}
