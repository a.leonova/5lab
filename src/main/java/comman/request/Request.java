package comman.request;

public class Request {
    private RequestType type;
    private String name;
    private String message;

    public Request createMessageRequest(String message){
        type = RequestType.MESSAGE;
        this.message = message;
        return this;
    }

    public Request createLoginRequest(String name){
        type = RequestType.LOGIN;
        this.name = name;
        return this;
    }

    public Request createLogout(){
        type = RequestType.LOGOUT;
        return this;
    }

    public RequestType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }
}
