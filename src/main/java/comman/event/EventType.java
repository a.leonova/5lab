package comman.event;

public enum EventType {
    MESSAGE,
    USER_JOIN,
    USER_LEFT
}
