package comman.event;

public class Event {
    private EventType type;
    private String name;
    private String message;

    public Event createMessageEvent(String name, String data){
        type = EventType.MESSAGE;
        this.name = name;
        this.message = data;
        return this;
    }

    public Event createJoinEvent(String name){
        type = EventType.USER_JOIN;
        this.name = name;
        return this;
    }

    public Event createLeftEvent(String name){
        type = EventType.USER_LEFT;
        this.name = name;
        return this;
    }

    public EventType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }
}
